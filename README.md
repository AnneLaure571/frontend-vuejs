# Frontend VueJS

Frontend en VueJS pour le filestore.


Ce Frontend reprend l'aspect graphique de l'ancien frontend Java fait avec Freemaker.

## Auteurs : groupe 1 (Charles, Massicard, Romanel, Yacia)


## Notes techniques

Nous avons utilisé le framework Vuetify pour les composants graphiques.


## Alternative
Le groupe 6 a également réalisé un frontend en VueJS, avec un aspect différent. Il semblerait cependant que leur projet soit moins avancé.

## Remarque
Si vous modifiez du code existant sur ce dépot, merci de ne pas pusher sur master et créez plutôt une nouvelle branche par groupe.

## Fonctionnement

Lorsque vous lancez le projet pour la première fois, vous devez exécuter "npm install" afin de récupérer les dépendances listées dans le fichier package.json.

Le fichier .env permet de paramétrer quelques variables. Modifiez-le si besoin.

Ensuite, vous pourrez lancer "npm run serve" pour lancer un serveur web de développement en local sur votre machine. Attention de bien lancer Keycloak **avant** de réaliser cette manip, sinon vous aurez des problèmes avec les ports !

Bien sûr le backend doit être lancé en parallèle. Il y a quelques classes en plus côté backend, notamment des filtres dans l'API et des modifs pour pouvoir récupérer le profil de l'utilisateur.

[Plus d'infos sur notre wiki](https://gitlab.com/miage.20/filestore-mediatheque/-/wikis/Utilisation-du-frontend-JS)


## Build docker image
docker build -t dokku/filestore-front:1.0.0 .

## Run docker container
docker run -e API_URL=<manager_api_url> -e KEYCLOAK_URL=http://localhost:8080/auth -e KEYCLOAK_REALM=filestore -e KEYCLOAK_CLIENT_ID=filestore dokku/filestore-front:1.0.0
