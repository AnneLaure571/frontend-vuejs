import axios from "axios";
import keycloak from "../plugins/keycloak";

const filestoreAPI = axios.create({
  baseURL:
    process.env.VUE_APP_NODE_ENV == "development"
      ? "http://" + process.env.VUE_APP_HOST_MANAGER + ":" + process.env.VUE_APP_API_PORT_MANAGER
      : "",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

const managerAPI = axios.create({
  baseURL:
    process.env.VUE_APP_NODE_ENV == "development"
      ? "http://" + process.env.VUE_APP_HOST_MANAGER + ":" + process.env.VUE_APP_API_PORT_MANAGER
      : "http://manager.files.jayblanc.fr",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

filestoreAPI.interceptors.request.use(
  (config) => {
    config.headers = {
      Authorization: "Bearer " + keycloak.token,
      Accept: "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

filestoreAPI.interceptors.response.use(
  (response) => {
    return response;
  },
  async function(error) {
    return Promise.reject(error);
  }
);

managerAPI.interceptors.request.use(
  (config) => {
    config.headers = {
      Authorization: "Bearer " + keycloak.token,
      Accept: "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

managerAPI.interceptors.response.use(
  (response) => {
    return response;
  },
  async function(error) {
    return Promise.reject(error);
  }
);

export default {
  getProfile: (url) => {
    return filestoreAPI.get("/api/profile", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getFile: (url, id = "") => {
    return filestoreAPI.get(`/api/files/${id}`, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getFileContent: (url, id) => {
    return filestoreAPI.get(`/api/files/${id}/content`, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getFileURL(url, id, download) {
    const baseUrl = process.env.VUE_APP_NODE_ENV == "development"
    ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
    : url;
    return baseUrl + "/api/files/" + id + "/content?download=" + download;
  },
  getNeighbours: (url) => {
    return filestoreAPI.get("/api/network", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getStatus: (url) => {
    return filestoreAPI.get("/api/status", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getImages: (url) => {
    return filestoreAPI.get("/api/images", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getMusics: (url) => {
    return filestoreAPI.get("/api/musics", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getVideos: (url) => {
    return filestoreAPI.get("/api/videos", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getThumbnail: (url, id) => {
    return filestoreAPI.get(`/api/images/${id}/thumbnail`, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  refreshImages: (url) => {
    return filestoreAPI.get("/api/images/refresh", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  refreshAudios: (url) => {
    return filestoreAPI.get("/api/musics/refresh", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  refreshVideos: (url) => {
    return filestoreAPI.get("/api/videos/refresh", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  uploadFile: (url, folderID, formData) => {
    return filestoreAPI.post(`/api/files/${folderID}`, formData, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  deleteFile: (url, id, name) => {
    return filestoreAPI.delete(`/api/files/${id}/${name}`, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  getAllTags(url) {
    return filestoreAPI.get("/api/images/tags", {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  addTag: (url, imageId, name) => {
    return filestoreAPI.post(`/api/images/${imageId}/tag/${name}`, null, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },
  removeTag: (url, imageId, name) => {
    return filestoreAPI.delete(`/api/images/${imageId}/tag/${name}`, {
      baseURL:
        process.env.VUE_APP_NODE_ENV == "development"
          ? "http://" + process.env.VUE_APP_HOST_FILESTORE + ":" + process.env.VUE_APP_API_PORT_FILESTORE
          : url,
    });
  },

  // ---------------------------------------------------------------------------

  getManagerProfile: (profileid) => {
    return managerAPI.get(`/api/profiles/${profileid}`);
  },
  getStore: (profileid) => {
    return managerAPI.get(`/api/profiles/${profileid}/store`);
  },
  createStore(profileid, data) {
    return managerAPI.post(`/api/profiles/${profileid}/store`, data, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  },
  destroyStore(profileid) {
    return managerAPI.delete(`/api/profiles/${profileid}/store`);
  },
};
