import Keycloak from "keycloak-js";

const keycloak = new Keycloak({
    url: process.env.VUE_APP_NODE_ENV == "development" ? process.env.VUE_APP_KEYCLOAK_URL : "http://auth.files.jayblanc.fr/auth",
    realm: process.env.VUE_APP_NODE_ENV == "development" ? process.env.VUE_APP_KEYCLOAK_REALM : "filestore",
    clientId: process.env.VUE_APP_NODE_ENV == "development" ? process.env.VUE_APP_KEYCLOAK_CLIENT_ID : "filestore",
});

export default keycloak;