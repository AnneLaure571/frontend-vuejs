#!/usr/bin/env sh

set -e

[ -z "$DEBUG" ] || set -x

# consts
APP_BASE=/usr/share/nginx/html

# vars
API_URL=${API_URL:-http://localhost:8280}
KEYCLOAK_URL=${KEYCLOAK_URL:-http://localhost:8080/auth}
KEYCLOAK_REALM=${KEYCLOAK_REALM:-filestore}
KEYCLOAK_CLIENTID=${KEYCLOAK_CLIENTID:-filestore}

# set KEYCLOAK url
echo Setting KEYCLOAK endpoint KEYCLOAK_URL
[ -n "$KEYCLOAK_URL" ]
find "$APP_BASE" -type f -exec sed -i "s|##KEYCLOAK_URL##|$KEYCLOAK_URL|g" {} \;
grep -qr "$KEYCLOAK_URL" "$APP_BASE"

# set KEYCLOAK realm
echo Setting KEYCLOAK realm KEYCLOAK_REALM
[ -n "$KEYCLOAK_REALM" ]
find "$APP_BASE" -type f -exec sed -i "s|##KEYCLOAK_REALM##|$KEYCLOAK_REALM|g" {} \;
grep -qr "$KEYCLOAK_REALM" "$APP_BASE"

# set KEYCLOAK clientid
echo Setting KEYCLOAK clientid KEYCLOAK_CLIENT_ID
[ -n "$KEYCLOAK_CLIENT_ID" ]
find "$APP_BASE" -type f -exec sed -i "s|##KEYCLOAK_CLIENT_ID##|$KEYCLOAK_CLIENT_ID|g" {} \;
grep -qr "$KEYCLOAK_CLIENT_ID" "$APP_BASE"

# set MANAGER endpoint
echo Setting MANAGER endpoint API_URL
[ -n "$API_URL" ]
find "$APP_BASE" -type f -exec sed -i "s|##API_URL##|$API_URL|g" {} \;
grep -qr "$API_URL" "$APP_BASE"

# go
exec nginx -g "daemon off;"
