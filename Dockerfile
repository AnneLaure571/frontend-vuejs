FROM node:14-alpine AS build-front
COPY src /usr/src/app/src
COPY public /usr/src/app
COPY package.json /usr/src/app
COPY vue.config.js /usr/src/app
COPY babel.config.js /usr/src/app
COPY docker/.env.template /usr/src/app/.env
WORKDIR /usr/src/app
RUN npm install
RUN npm run build

FROM nginx:stable-alpine AS run
COPY --from=build-front /usr/src/app/dist /usr/share/nginx/html
COPY docker /
EXPOSE 80
ENTRYPOINT ["/scripts/docker-entrypoint.sh"]
