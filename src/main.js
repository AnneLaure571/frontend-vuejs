import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./plugins/router";
import store from "./plugins/store";
import keycloak from "./plugins/keycloak";
import VueLogger from 'vuejs-logger';
import Snotify, {
    SnotifyPosition
} from 'vue-snotify';

import "vue-snotify/styles/material.css";
import '@mdi/font/css/materialdesignicons.css';

Vue.config.productionTip = false;
const isProduction = process.env.NODE_ENV === 'production';

Vue.use(Snotify, {
    toast: {
        position: SnotifyPosition.centerBottom,
        closeOnClick: true,
        timeout: 3000,
        showProgressBar: false,
        pauseOnHover: true,
        maxOnScreen: 3,
        preventDuplicates: true
    }
});

const logOptions = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
};

Vue.use(VueLogger, logOptions);

keycloak.init({
    onLoad: "login-required"
}).then((auth) => {
    if (!auth) {
        window.location.reload();
    } else {
        Vue.$log.info("Authenticated");
        store.commit('setProfileId', keycloak.subject);

        new Vue({
            el: '#app',
            vuetify,
            store,
            router,
            render: h => h(App, {
                props: {
                    keycloak: keycloak
                }
            })
        })
    }

    //Token Refresh
    setInterval(() => {
        keycloak.updateToken(70).then((refreshed) => {
            if (!refreshed) {
                Vue.$log.warn('Token not refreshed, valid for ' +
                    Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
            }
        }).catch(() => {
            Vue.$log.error('Failed to refresh token');
        });
    }, 6000)

}).catch(() => {
    Vue.$log.error("Authenticated Failed");
});