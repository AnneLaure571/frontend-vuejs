import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

const store = new Vuex.Store({

    plugins: [createPersistedState()],
    
    state: {
        profileId: null,
        profile: null,
        store: null
    },

    mutations: {
        setProfileId(state, profileId) {
            state.profileId = profileId;
        },

        setProfile(state, profile) {
            state.profile = profile;
        },

        setStore(state,
            store) {
            state.store = store;

        }
    },

    actions: {
        setProfileId(context, profileId) {
            context.commit('setProfileId', profileId);
        },

        setProfile(context, profile) {
            context.commit('setProfile', profile);
        },

        setStore(context, store) {
            context.commit('setStore', store);
        },
    }
})

export default store