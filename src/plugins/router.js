import Vue from 'vue'
import VueRouter from 'vue-router'
import Profile from '../views/Profile.vue'
import Files from '../views/Files.vue'
import Shares from '../views/Shares.vue'
import Neighbors from '../views/Neighbors.vue'
import Status from '../views/Status.vue'
import Images from '../views/Images.vue'
import Musics from '../views/Musics.vue'
import Videos from '../views/Videos.vue'

Vue.use(VueRouter)

const routes = [{
        path: "/profile",
        component: Profile,
        name: "profile",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/files",
        component: Files,
        name: "files",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/shares",
        component: Shares,
        name: "shares",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/neighbors",
        component: Neighbors,
        name: "neighbors",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/status",
        component: Status,
        name: "status",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/images",
        component: Images,
        name: "images",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/musics",
        component: Musics,
        name: "musics",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/videos",
        component: Videos,
        name: "video",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "*",
        redirect: "/profile",
        meta: {
            requiresAuth: true
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router