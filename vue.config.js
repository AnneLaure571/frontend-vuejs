module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "Filestore";
                return args;
            })
    },
    "transpileDependencies": [
        "vuetify"
    ],
    devServer: {
        port: 8081
    }
}