import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: "#32BCE7",
                secondary: colors.grey.darken1,
                accent: colors.indigo.base,
                background1: "#FAFBFC",
                background2: "#F5F7FA",
            },
        },
    },
});